<?php declare(strict_types=1);
namespace Crunch\FastCGI\Protocol;

use Crunch\FastCGI\ReaderWriter\ReaderInterface;

interface ResponseInterface
{
    public function getContent(): ReaderInterface;

    public function getError(): ReaderInterface;
}
