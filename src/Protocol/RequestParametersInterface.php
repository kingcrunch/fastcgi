<?php declare(strict_types=1);
namespace Crunch\FastCGI\Protocol;

/**
 * Representing the request parameters.
 */
interface RequestParametersInterface
{
    public function encode(int $requestId): iterable;
}
