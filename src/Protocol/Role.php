<?php declare(strict_types=1);
namespace Crunch\FastCGI\Protocol;

use InvalidArgumentException;

/**
 * Type: Role.
 */
class Role
{
    public const RESPONDER = 1;
    public const AUTHORIZER = 2;
    public const FILTER = 3;

    private int $role;

    /** @var Role[] */
    private static array $instances = [];

    private function __construct(int $role)
    {
        $this->role = $role;
    }

    /**
     * Returns the raw value.
     */
    public function value(): int
    {
        return $this->role;
    }

    /**
     * Returns an instance of the given role.
     *
     * @throws InvalidArgumentException
     */
    public static function instance(int $role): Role
    {
        // @codeCoverageIgnoreStart
        if (!self::$instances) {
            self::$instances = [
                self::RESPONDER => new self(self::RESPONDER),
                self::AUTHORIZER => new self(self::AUTHORIZER),
                self::FILTER => new self(self::FILTER),
            ];
        }
        // @codeCoverageIgnoreEnd

        if (!array_key_exists($role, self::$instances)) {
            throw new InvalidArgumentException("Invalid Role $role");
        }

        return self::$instances[$role];
    }

    public function isResponder(): bool
    {
        return $this->role === self::RESPONDER;
    }

    public function isAuthorizer(): bool
    {
        return $this->role === self::AUTHORIZER;
    }

    public function isFilter(): bool
    {
        return $this->role === self::FILTER;
    }

    public static function responder(): self
    {
        return self::instance(self::RESPONDER);
    }

    public static function authorizer(): self
    {
        return self::instance(self::AUTHORIZER);
    }

    public static function filter(): self
    {
        return self::instance(self::FILTER);
    }
}
