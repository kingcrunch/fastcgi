<?php declare(strict_types=1);
namespace Crunch\FastCGI\Protocol;

use Traversable;

interface RequestInterface
{
    /**
     * Encodes request into an `iterable` of records.
     *
     * @return iterable
     */
    public function toRecords(): iterable;
}
