<?php declare(strict_types=1);
namespace Crunch\FastCGI\Protocol;

use ArrayIterator;
use Crunch\FastCGI\ReaderWriter\EmptyReader;
use Crunch\FastCGI\ReaderWriter\ReaderInterface;

class Request implements RequestInterface
{
    private Role $role;
    private int $requestId;
    private bool $keepConnection;
    private RequestParameters $parameters;
    private ReaderInterface $stdin;

    /**
     * Creates new Request instance.
     *
     * If $keepConnection is set to `false` the server may close the connection
     * right after sending the response.
     *
     * @param Role $role
     * @param int $requestId
     * @param bool $keepConnection Default: true
     * @param RequestParametersInterface|null $parameters
     * @param ReaderInterface|null $stdin
     */
    public function __construct(Role $role, int $requestId, ?bool $keepConnection = true, ?RequestParametersInterface $parameters = null, ?ReaderInterface $stdin = null)
    {
        $this->role = $role;
        $this->requestId = $requestId;
        $this->keepConnection = $keepConnection;
        $this->parameters = $parameters ?: new RequestParameters();
        $this->stdin = $stdin ?: new EmptyReader();
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    public function getRequestId(): int
    {
        return $this->requestId;
    }

    public function isKeepConnection(): bool
    {
        return $this->keepConnection;
    }

    public function getParameters(): RequestParameters
    {
        return $this->parameters;
    }

    public function getStdin(): ReaderInterface
    {
        return $this->stdin;
    }

    /**
     * Encodes request into an `iterable` of records.
     *
     * @return iterable
     */
    public function toRecords(): iterable
    {
        $result = [new Record(new Header(RecordType::beginRequest(), $this->getRequestId(), 8), pack('xCCxxxxx', $this->role->value(), 0xFF & ($this->keepConnection ? 1 : 0)))];

        foreach ($this->getParameters()->encode($this->getRequestId()) as $value) {
            $result[] = $value;
        }

        while ($chunk = $this->stdin->read(65535)) {
            $result[] = new Record(new Header(RecordType::stdin(), $this->getRequestId(), strlen($chunk)), $chunk);
        }

        $result[] = new Record(new Header(RecordType::stdin(), $this->getRequestId(), 0, 0), '');

        return new ArrayIterator($result);
    }
}
