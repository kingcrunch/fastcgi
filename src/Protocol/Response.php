<?php declare(strict_types=1);
namespace Crunch\FastCGI\Protocol;

use ArrayIterator;
use Crunch\FastCGI\ReaderWriter\ReaderInterface;

class Response implements ResponseInterface
{
    private int $requestId;
    /** @var ReaderInterface */
    private ReaderInterface $content;
    /** @var ReaderInterface */
    private ReaderInterface $error;

    /**
     * @param int $requestId
     * @param ReaderInterface $content
     * @param ReaderInterface $error
     */
    public function __construct(int $requestId, ReaderInterface $content, ReaderInterface $error)
    {
        $this->requestId = $requestId;
        $this->content = $content;
        $this->error = $error;
    }

    public function getRequestId(): int
    {
        return $this->requestId;
    }

    public function getContent(): ReaderInterface
    {
        return $this->content;
    }

    public function getError(): ReaderInterface
    {
        return $this->error;
    }

    /**
     * Encodes request into am `iterable` of records.
     */
    public function toRecords(): iterable
    {
        $result = [];

        while ($chunk = $this->error->read(65535)) {
            $result[] = new Record(new Header(RecordType::stderr(), $this->requestId, strlen($chunk)), $chunk);
        }

        while ($chunk = $this->content->read(65535)) {
            $result[] = new Record(new Header(RecordType::stdout(), $this->requestId, strlen($chunk)), $chunk);
        }
        $result[] = new Record(new Header(RecordType::stdout(), $this->requestId, 0, 8), '');

        $result[] = new Record(new Header(RecordType::endRequest(), $this->requestId, 0, 8), '');

        return new ArrayIterator($result);
    }
}
