<?php declare(strict_types=1);
namespace Crunch\FastCGI\Protocol;

use InvalidArgumentException;
use LengthException;

/**
 * Record.
 *
 * The record is the smallest unit in the communication between the
 * server and the client. It consists of the Header and the payload, which
 * itself consists of the actual data and some padding zero-bytes.
 */
class Record
{
    public const BEGIN_REQUEST = 1;
    public const ABORT_REQUEST = 2;
    public const END_REQUEST = 3;
    public const PARAMS = 4;
    public const STDIN = 5;
    public const STDOUT = 6;
    public const STDERR = 7;
    public const DATA = 8;
    public const GET_VALUES = 9;
    public const GET_VALUES_RESULT = 10;
    public const UNKNOWN_TYPE = 11;
    public const MAXTYPE = self::UNKNOWN_TYPE;

    private Header $header;
    /** Content received */
    private string $content;

    /**
     * @param Header $header
     * @param string $content
     * @throws InvalidArgumentException Thrown when $content is not a string
     * @throws LengthException          Thrown when the length of the content does
     *                                  not match the length given by the header
     */
    public function __construct(Header $header, string $content)
    {
        if (strlen($content) !== $header->getLength()) {
            throw new LengthException('The length of the content must match the length propagated by the header');
        }

        $this->header = $header;
        $this->content = $content;
    }

    /**
     * Compiles record into struct to send.
     */
    public function encode(): string
    {
        return $this->header->encode() . $this->getContent() . str_repeat("\0", $this->header->getPaddingLength());
    }

    public static function decode(Header $header, string $payload): self
    {
        return new self($header, $payload);
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    public function getRequestId(): int
    {
        return $this->header->getRequestId();
    }

    public function getType(): RecordType
    {
        return $this->header->getType();
    }
}
