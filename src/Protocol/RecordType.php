<?php declare(strict_types=1);
namespace Crunch\FastCGI\Protocol;

class RecordType
{
    public const BEGIN_REQUEST = 1;
    public const ABORT_REQUEST = 2;
    public const END_REQUEST = 3;
    public const PARAMS = 4;
    public const STDIN = 5;
    public const STDOUT = 6;
    public const STDERR = 7;
    public const DATA = 8;
    public const GET_VALUES = 9;
    public const GET_VALUES_RESULT = 10;
    public const UNKNOWN_TYPE = 11;
    public const MAXTYPE = self::UNKNOWN_TYPE;

    private int $type;

    private static array $instances = [];

    /**
     * @param int $type
     */
    private function __construct(int $type)
    {
        $this->type = $type;
    }

    public function value(): int
    {
        return $this->type;
    }

    public static function instance(int $type): RecordType
    {
        // @codeCoverageIgnoreStart
        if (!self::$instances) {
            self::$instances = [
                self::BEGIN_REQUEST     => new self(self::BEGIN_REQUEST),
                self::ABORT_REQUEST     => new self(self::ABORT_REQUEST),
                self::END_REQUEST       => new self(self::END_REQUEST),
                self::PARAMS            => new self(self::PARAMS),
                self::STDIN             => new self(self::STDIN),
                self::STDOUT            => new self(self::STDOUT),
                self::STDERR            => new self(self::STDERR),
                self::DATA              => new self(self::DATA),
                self::GET_VALUES        => new self(self::GET_VALUES),
                self::GET_VALUES_RESULT => new self(self::GET_VALUES_RESULT),
                self::UNKNOWN_TYPE      => new self(self::UNKNOWN_TYPE),
            ];
        }
        // @codeCoverageIgnoreEnd

        if ($type >= self::MAXTYPE || $type <= 0) {
            $type = self::UNKNOWN_TYPE;
        }

        return self::$instances[$type];
    }

    public function isBeginRequest(): bool
    {
        return $this->value() === self::instance(self::BEGIN_REQUEST)->value();
    }

    public function isAbortRequest(): bool
    {
        return $this->value() === self::instance(self::ABORT_REQUEST)->value();
    }

    public function isEndRequest(): bool
    {
        return $this->value() === self::instance(self::END_REQUEST)->value();
    }

    public function isParams(): bool
    {
        return $this->value() === self::instance(self::PARAMS)->value();
    }

    public function isStdin(): bool
    {
        return $this->value() === self::instance(self::STDIN)->value();
    }

    public function isStdout(): bool
    {
        return $this->value() === self::instance(self::STDOUT)->value();
    }

    public function isStderr(): bool
    {
        return $this->value() === self::instance(self::STDERR)->value();
    }

    public function isData(): bool
    {
        return $this->value() === self::instance(self::DATA)->value();
    }

    public function isGetValues(): bool
    {
        return $this->value() === self::instance(self::GET_VALUES)->value();
    }

    public function isGetValuesResult(): bool
    {
        return $this->value() === self::instance(self::GET_VALUES_RESULT)->value();
    }

    public function isUnknownType(): bool
    {
        return $this->value() === self::instance(self::UNKNOWN_TYPE)->value();
    }

    public function isMaxtype(): bool
    {
        return $this->value() === self::instance(self::MAXTYPE)->value();
    }

    public static function beginRequest(): self
    {
        return  self::instance(self::BEGIN_REQUEST);
    }

    public static function abortRequest(): self
    {
        return self::instance(self::ABORT_REQUEST);
    }

    public static function endRequest(): self
    {
        return self::instance(self::END_REQUEST);
    }

    public static function params(): self
    {
        return self::instance(self::PARAMS);
    }

    public static function stdin(): self
    {
        return self::instance(self::STDIN);
    }

    public static function stdout(): self
    {
        return self::instance(self::STDOUT);
    }

    public static function stderr(): self
    {
        return self::instance(self::STDERR);
    }

    public static function data(): self
    {
        return self::instance(self::DATA);
    }

    public static function getValues(): self
    {
        return self::instance(self::GET_VALUES);
    }

    public static function getValuesResult(): self
    {
        return self::instance(self::GET_VALUES_RESULT);
    }

    public static function unknownType(): self
    {
        return self::instance(self::UNKNOWN_TYPE);
    }

    public static function maxtype(): self
    {
        return self::instance(self::UNKNOWN_TYPE);
    }
}
