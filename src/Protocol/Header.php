<?php declare(strict_types=1);
namespace Crunch\FastCGI\Protocol;

use DomainException;
use InvalidArgumentException;
use LengthException;

/**
 * Record header.
 *
 * Speaking of the actual transmission the header is the first 8 byte sequence
 * of the byte stream. It contains the metadata consisting of the FastCGI-version
 * (constant "1"), the type of the record, the request ID, the length of the
 * content and the length of the padding sequence.
 *
 * The request ID exists to identify which response belongs to which request
 * for the server and (when multiplexing) for the client.
 *
 * The padding is zero-byte sequence. With the actual record -- the
 * header, the content and the payload -- is always divisible by 8. However,
 * the specification allows a padding of up to 255 bytes.
 *
 * The content length is limited to 65535 bytes.
 *
 * The type defines one of the 11 record types (including "Unknown type" 11).
 */
class Header
{
    private RecordType $type;
    private int $requestId;
    private int $length;
    private int $paddingLength;

    /**
     * Header constructor.
     *
     * If $paddingLength is omitted, it is calculated from $length. If $paddingLength
     * is set, it will be validated against $length.
     *
     * @param RecordType $type
     * @param int $requestId Greater than 1
     * @param int $length Must be between 0 and 65535 (including)
     * @param int|null $paddingLength between 0 and 7
     *                                  Calculated from $length when omitted, exception when invalid
     * @throws InvalidArgumentException thrown when one argument is of an invalid type
     * @throws DomainException thrown when the value of an argument is invalid
     */
    public function __construct(RecordType $type, int $requestId, int $length, ?int $paddingLength = null)
    {
        if ($requestId <= 0) {
            throw new DomainException("Request ID must be a positive integer, $requestId given");
        }

        if (0 > $length || $length > 65535) {
            throw new DomainException("Length must be between 0 and 65535, $length given");
        }

        if (!is_null($paddingLength) && !is_int($paddingLength)) {
            throw new InvalidArgumentException(sprintf('Padding length must be an integer, or null, %s given', gettype($paddingLength)));
        }
        if (!is_null($paddingLength) && (0 > $paddingLength || $paddingLength > 255)) {
            throw new DomainException("Padding length must be null or between 0 and 255, $length given");
        }
        if (!is_null($paddingLength) && ($paddingLength + $length) % 8 !== 0) {
            throw new DomainException(sprintf('Padding length must be null or Padding Length + Length must be divisible by 8, %d given', $paddingLength + $length));
        }

        $this->type = $type;
        $this->requestId = $requestId;
        $this->length = $length;

        if (!$paddingLength) {
            /* Although it is undocumented it seems, that a padding of 0 may end up
             * in a deadlock. I have tested it against php-fpm and while it worked
             * with unix sockets the server doesn't start to send any response when
             * using TCP-sockets and there are records without content and no padding.
             */
            $paddingLength = (8 - ($length % 8)) % 8;
        }
        $this->paddingLength = (int)$paddingLength;
    }

    /**
     * @param string $header
     *
     * @return Header
     * @throws LengthException thrown when $header is not exactly 8 bytes
     * @throws InvalidArgumentException thrown when $header is not a string
     */
    public static function decode(string $header): Header
    {
        if (strlen($header) !== 8) {
            throw new LengthException(sprintf('Header must be exactly 8 bytes, %d bytes given', strlen($header)));
        }

        $headers = unpack('Cversion/Ctype/nrequestId/nlength/CpaddingLength/Creserved', $header);

        return new self(RecordType::instance($headers['type']), $headers['requestId'], $headers['length'], $headers['paddingLength']);
    }

    public function getType(): RecordType
    {
        return $this->type;
    }

    public function getRequestId(): int
    {
        return $this->requestId;
    }

    /**
     * @return int Content length between 0 and 65535 (including)
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @return int Padding length between 0 and 7 (including)
     */
    public function getPaddingLength(): int
    {
        return $this->paddingLength;
    }

    /**
     * @return int Length of the entire payload between 0 and 65535 (including)
     */
    public function getPayloadLength(): int
    {
        return $this->getLength() + $this->getPaddingLength();
    }

    /**
     * Returns the encoded header as a string.
     *
     * @return string
     */
    public function encode(): string
    {
        return pack('CCnnCx', 1, $this->getType()->value(), $this->getRequestId(), $this->getLength(), $this->getPaddingLength());
    }
}
