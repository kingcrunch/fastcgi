<?php declare(strict_types=1);

namespace Crunch\FastCGI\Client;

use Crunch\FastCGI\Protocol\Record;
use Crunch\FastCGI\Protocol\Response;
use Crunch\FastCGI\ReaderWriter\StringReader;
use RuntimeException;

class ResponseParser
{
    private int $requestId;
    private string $stdout = '';
    private string $stderr = '';

    public function __construct(int $requestId)
    {
        $this->requestId = $requestId;
    }

    /**
     * @throws RuntimeException
     */
    public function pushRecord(Record $record): ?Response
    {
        switch (true) {
            case $record->getType()->isStdout():
                $this->stdout .= $record->getContent();
                break;
            case $record->getType()->isStderr():
                $this->stderr .= $record->getContent();
                break;
            case $record->getType()->isEndRequest():
                return new Response($this->requestId, new StringReader($this->stdout), new StringReader($this->stderr));
            default:
                throw new RuntimeException(sprintf('Unknown package type \'%d\'', $record->getType()));
        }
        return null;
    }
}
