<?php declare(strict_types=1);
namespace Crunch\FastCGI\Client;

use RuntimeException;

class ClientException extends RuntimeException
{
}
