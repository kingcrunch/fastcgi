<?php declare(strict_types=1);
namespace Crunch\FastCGI\Client;

use Crunch\FastCGI\Protocol\RequestInterface;
use Crunch\FastCGI\Protocol\RequestParametersInterface;
use Crunch\FastCGI\Protocol\ResponseInterface;
use Crunch\FastCGI\ReaderWriter\ReaderInterface;
use React\Promise\PromiseInterface;

interface ClientInterface
{
    /**
     * Creates a new request.
     *
     * Although you can create a Request instance manually it is highly
     * recommended to use this factory method, because only this one
     * ensures, that the request uses a previously unused request id.
     *
     * @param RequestParametersInterface|null $parameters
     * @param ReaderInterface|null            $stdin
     */
    public function newRequest(RequestParametersInterface $parameters = null, ReaderInterface $stdin = null): RequestInterface;

    /**
     * Send request, but don't wait for response.
     *
     * Remember to call receiveResponse(). Else, it will remain the buffer.
     */
    public function sendRequest(RequestInterface $request): PromiseInterface;
}
