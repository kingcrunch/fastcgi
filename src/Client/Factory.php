<?php declare(strict_types=1);
namespace Crunch\FastCGI\Client;

use React\Promise\PromiseInterface;
use React\Socket\ConnectorInterface;
use React\Stream\DuplexStreamInterface;

class Factory
{
    private ConnectorInterface $connector;

    /**
     * Factory constructor.
     *
     * @param ConnectorInterface $connector
     */
    public function __construct(ConnectorInterface $connector)
    {
        $this->connector = $connector;
    }

    public function createClient(string $host, int $port): PromiseInterface
    {
        return $this->connector->connect("$host:$port")->then(function (DuplexStreamInterface $stream) {
            return new Client($stream);
        });
    }
}
