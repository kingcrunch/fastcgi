<?php declare(strict_types=1);
namespace Crunch\FastCGI\Server;

use Crunch\FastCGI\Protocol\Record;
use Crunch\FastCGI\Protocol\Request;
use Crunch\FastCGI\Protocol\RequestInterface;
use Crunch\FastCGI\Protocol\RequestParameters;
use Crunch\FastCGI\Protocol\Role;
use Crunch\FastCGI\ReaderWriter\StringReader;
use RuntimeException;

class RequestParser
{
    /** @var Record[] */
    private array $records = [];

    public function pushRecord(Record $record): ?RequestInterface
    {
        $this->records[] = $record;

        if ($record->getContent() || !$record->getType()->isStdin()) {
            return null;
        }

        return $this->buildRequest();
    }

    private function buildRequest(): RequestInterface
    {
        /** @var Record $record */
        $record = array_shift($this->records);
        $role = Role::instance(ord($record->getContent()[1]));
        $keepConnection = $record->getContent()[2] !== "\x00";

        $params = '';
        while ($this->records && $this->records[0]->getType()->isParams()) {
            /** @var Record $record */
            $record = array_shift($this->records);

            $params .= $record->getContent();
        }

        $stdin = '';
        while ($this->records && $this->records[0]->getType()->isStdin()) {
            /** @var Record $record */
            $record = array_shift($this->records);

            $stdin .= $record->getContent();
        }

        if ($this->records) {
            throw new RuntimeException('recordset not empty. Something went wrong');
        }

        return new Request($role, $record->getRequestId(), $keepConnection, RequestParameters::decode($params), new StringReader($stdin));
    }
}
