<?php declare(strict_types=1);
namespace Crunch\FastCGI\ReaderWriter;

class StringReader implements ReaderInterface
{
    private $data;
    private int $remaining;

    /**
     * @param string $data
     */
    public function __construct(string $data)
    {
        $this->remaining = strlen($data);

        $this->data = fopen('php://temp', 'w+b');
        fwrite($this->data, $data);
        fseek($this->data, 0);
    }

    public function __destruct()
    {
        $this->close();
    }

    public function read(?int $max = null): string
    {
        $max = min($max ?: PHP_INT_MAX, $this->remaining);
        if ($this->remaining <= 0 || feof($this->data)) {
            $this->close();
            return '';
        }
        $this->remaining -= $max;

        return fread($this->data, $max);
    }

    private function close(): void
    {
        if (is_resource($this->data)) {
            fclose($this->data);
        }
    }
}
