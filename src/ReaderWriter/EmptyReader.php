<?php declare(strict_types=1);
namespace Crunch\FastCGI\ReaderWriter;

class EmptyReader implements ReaderInterface
{
    public function read($max = null): string
    {
        return '';
    }
}
