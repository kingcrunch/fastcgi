<?php declare(strict_types=1);
namespace Crunch\FastCGI\ReaderWriter;

class VoidWriter implements WriterInterface
{
    public function write(string $data): void
    {
        // NOOP
    }
}
