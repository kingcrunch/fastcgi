<?php declare(strict_types=1);
namespace Crunch\FastCGI\ReaderWriter;

interface WriterInterface
{
    /**
     * @param string $data
     *
     * @return void
     */
    public function write(string $data): void;
}
