<?php declare(strict_types=1);
namespace Crunch\FastCGI\Protocol;

use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Crunch\FastCGI\Protocol\Record
 * @covers \Crunch\FastCGI\Protocol\Record
 */
class RecordTest extends TestCase
{
    /**
     * @covers ::getType
     */
    public function testInstanceKeepsType()
    {
        $header = $this->getMockBuilder(Header::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getLength', 'getType'])
            ->getMock();
        $header->expects($this->atLeast(1))
            ->method('getLength')
            ->willReturn(3);
        $header->expects($this->atLeast(1))
            ->method('getType');

        $record = new Record($header, 'foo');
        $record->getType();
    }

    /**
     * @covers ::getRequestId
     */
    public function testInstanceKeepsRequestId()
    {
        $header = $this->getMockBuilder(Header::class)
            ->setConstructorArgs([RecordType::beginRequest(), 12, 13])
            ->onlyMethods(['getLength'])
            ->getMock();
        $header->expects($this->atLeast(1))
            ->method('getLength')
            ->willReturn(3);

        $record = new Record($header, 'foo');
        $record->getRequestId();
    }

    /**
     * @covers ::getContent
     */
    public function testInstanceKeepsBody()
    {

        $header = $this->getMockBuilder(Header::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getLength'])
            ->getMock();
        $header->expects($this->atLeast(1))
            ->method('getLength')
            ->willReturn(3);

        $record = new Record($header, 'foo');

        self::assertEquals('foo', $record->getContent());
    }

    public static function invalidPayloadTypes()
    {
        return [
            /* $payload */
            [1, ''],
            [12, 'toshort'],
            [1, 'tolong'],
        ];
    }

    /**
     * @dataProvider invalidPayloadTypes
     * @param int $length
     * @param string $payload
     * @uses         \Crunch\FastCGI\Protocol\RecordType
     * @uses         \Crunch\FastCGI\Protocol\Header
     */
    public function testInvalidPayloadTypes(int $length, string $payload)
    {
        $this->expectException(\LengthException::class);
        $header = new Header(RecordType::beginRequest(), 1, $length);

        new Record($header, $payload);
    }

    public static function payloadLengthMismatchExamples()
    {
        return [
            /* $length, $payload */
            [0, 'abc'],
            [3, ''],
            [2, 'abcdef'],
        ];
    }


    /**
     * @dataProvider payloadLengthMismatchExamples
     * @param integer $length
     * @param string $payload
     * @uses         \Crunch\FastCGI\Protocol\Header
     * @uses         \Crunch\FastCGI\Protocol\RecordType
     */
    public function testPayloadLengthMismatch($length, $payload)
    {
        $this->expectException(\LengthException::class);
        $header = new Header(RecordType::beginRequest(), 1, $length);

        new Record($header, $payload);
    }
}
