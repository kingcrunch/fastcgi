<?php declare(strict_types=1);
namespace Crunch\FastCGI\Protocol;

use DomainException;
use LengthException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Crunch\FastCGI\Protocol\Header
 * @covers \Crunch\FastCGI\Protocol\Header
 */
class HeaderTest extends TestCase
{
    public static function validConstructorArguments(): array
    {
        return [
            /* $type, $requestId, $length, $padding */
            [RecordType::instance(4), 12, 4, null],
            [RecordType::instance(4), 12, 2, 6],
            [RecordType::instance(4), 12, 32, 0],
        ];
    }

    /**
     * @covers ::__construct
     * @dataProvider validConstructorArguments
     * @param RecordType $type
     * @param int $requestId
     * @param int $length
     * @param ?int $paddingLength
     */
    public function testConstructKeepsValues(RecordType $type, int $requestId, int $length, ?int $paddingLength): void
    {
        $header = new Header($type, $requestId, $length, $paddingLength);

        self::assertSame($type, $header->getType());
        self::assertEquals($requestId, $header->getRequestId());
        self::assertEquals($length, $header->getLength());
        if ($paddingLength) {
            self::assertEquals($paddingLength, $header->getPaddingLength());
            self::assertEquals($length + $paddingLength, $header->getPayloadLength());
        }
        self::assertEquals(0, $header->getPayloadLength() % 8);
    }

    public static function invalidConstructorArguments(): array
    {
        return [
            /* $exception, $type, $requestId, $length, $padding */
            [DomainException::class, RecordType::instance(4), 12, 4, 9],
            [DomainException::class, RecordType::instance(4), 12, 4, 2],
        ];
    }

    /**
     * @covers ::__construct
     * @dataProvider invalidConstructorArguments
     */
    public function testInvalidConstructorArguments(string $exception, RecordType $type, int $requestId, int $length, ?int $padding): void
    {
        $this->expectException($exception);
        new Header($type, $requestId, $length, $padding);
    }

    // TODO test invalid values (exception)

    public static function lengthAndPaddingProvider(): array
    {
        return [
            /* $length, $expectedPadding */
            [0, 0],
            [4, 4],
            [1, 7],
            [8, 0],
        ];
    }

    /**
     * @dataProvider lengthAndPaddingProvider
     * @param int $length
     * @param int $expectedPadding
     * @uses         RecordType
     * @covers ::__construct
     */
    public function testCorrectPaddingCalculation(int $length, int $expectedPadding): void
    {
        $header = new Header(RecordType::instance(2), 3, $length);

        self::assertEquals($expectedPadding, $header->getPaddingLength());
    }

    public static function encodedHeaderProvider(): array
    {
        /*
         * First byte "version"
         * Second byte "type"
         * Byte 3 and 4 "request id"
         * Byte 5 and 6 "length"
         * Byte 7 "paddingLength"
         * Byte 8 "unused" (still required)
         */
        return [
            /* $header, $type, $requestId, $length, $paddingLength */
            ["\x01\x02\x00\x03\x00\x04\x04\x00", RecordType::instance(2), 3, 4, 4],
            ["\x01\x06\x00\x12\x00\x10\x00\x00", RecordType::instance(6), 18, 16, 0],
            ["\x01\x06\x00\x12\x1f\xa4\x04\x00", RecordType::instance(6), 18, 8100, 4],
        ];
    }

    /**
     * @dataProvider encodedHeaderProvider
     * @param string $headerString
     * @param RecordType $type
     * @param int $requestId
     * @param int $length
     * @param int $paddingLength
     * @uses         RecordType
     * @covers ::decode
     */
    public function testDecodeHeader(string $headerString, RecordType $type, int $requestId, int $length, int $paddingLength): void
    {
        $header = Header::decode($headerString);

        self::assertSame($type, $header->getType());
        self::assertEquals($requestId, $header->getRequestId());
        self::assertEquals($length, $header->getLength());
        self::assertEquals($paddingLength, $header->getPaddingLength());
        self::assertEquals($length + $paddingLength, $header->getPayloadLength());
        self::assertEquals(0, $header->getPayloadLength() % 8);
    }

    /**
     * @dataProvider encodedHeaderProvider
     * @param string $headerString
     * @param RecordType $type
     * @param int $requestId
     * @param int $length
     * @param int $paddingLength
     * @uses         RecordType
     * @covers ::encode
     */
    public function testEncodeHeader(string $headerString, RecordType $type, int $requestId, int $length, int $paddingLength): void
    {
        $header = new Header($type, $requestId, $length, $paddingLength);

        self::assertEquals($headerString, $header->encode());
    }

    public static function invalidHeaderStrings(): array
    {
        return [
            /* $exception, $headerString */

            // too short
            [LengthException::class, "\x01"],
            [LengthException::class, "\x01\x02"],
            [LengthException::class, "\x01\x02\x00"],
            [LengthException::class, "\x01\x02\x00\x03"],
            [LengthException::class, "\x01\x02\x00\x03\x00"],
            [LengthException::class, "\x01\x02\x00\x03\x00\x04"],
            [LengthException::class, "\x01\x02\x00\x03\x00\x04\x05"],

            // too long
            [LengthException::class, "\x01\x02\x00\x03\x00\x04\x04\x06\x07"],
            [LengthException::class, "\x01\x02\x00\x03\x00\x04\x04\x06\x07\x08"],

            [LengthException::class, "\x01\x02\x00\x03\x00\x04\x09\x00"], // Invalid padding
        ];
    }
}
